#include<iostream>
#include<string>
using namespace std;

namespace collection
{
	class Exception
	{
	private:
		string message;
	public:
		Exception( string message = " " ) throw( ) : message( message )
		{	}
		string getMessage( void )const throw( )
		{
			return this->message;
		}
	};
	class LinkedList;	//Forward declration
	class Node
	{
	private:
		Node *prev;
		int data;
		Node *next;
	public:
		Node( int data = 0 ) throw( ) : prev( NULL ), data( data ), next( NULL )
		{	}
		friend class LinkedList;
	};
	class LinkedList
	{
	private:
		Node *head;
		Node *tail;
	public:
		LinkedList( void )throw( ) : head( NULL ), tail( NULL )
		{	}
		bool empty( void )const throw( )
		{
			return this->head == NULL;
		}

		void addFirst( int data )throw( bad_alloc )
		{
			Node *newNode  = new Node( data );
			if( this->empty( ) ){
				this->tail = newNode;
			}
			else
			{
				newNode->next = this->head;
				this->head->prev = newNode;
			}
			this->head = newNode;
			//newNode->next=this->head;
			newNode->prev=tail;
		}


		void addLast( int data )throw( bad_alloc )
		{
			Node *newNode  = new Node( data );
			if( this->empty( ) )
				this->head = newNode;
			else
			{
				this->tail->next = newNode;
				newNode->prev = this->tail;
			}
			this->tail = newNode;
			newNode->next=this->head;this->head->prev=newNode;
		}

		int nodeCount( void )const throw( )
		{
			int count = 0;
			Node *trav = this->head;
			do
			{
				++ count;
				trav = trav->next;
			}while( trav != this->head );
			return count;
		}
		void addAtPosition( int position, int data )throw( bad_alloc, Exception )
		{
			if( position <= 0 )
				throw Exception( "Invalid position" );
			else if( position == 1 )
				this->addFirst(data);
			else if( position > this->nodeCount( ) )
				this->addLast(data);
			else
			{
				Node *trav = this->head;
				for( int count = 1; count < position - 1; ++ count )
					trav = trav->next;

				Node *newNode = new Node( data );
				newNode->next = trav->next;
				trav->next->prev = newNode;
				trav->next = newNode;
				newNode->prev = trav;
			}
		}
		void removeFirst( void )throw( Exception )
		{
			if( this->empty( ) )
				throw Exception( "LinkedList is empty" );
			else if( this->head == this->tail )
			{
				delete this->head;
				this->head = this->tail = NULL;
			}
			else
			{
				Node *ptrNode = this->head;
				this->head = this->head->next;
				delete ptrNode;
				this->head->prev = this->tail;
			}
		}

		void removeLast( void )throw( Exception )
		{
			if( this->empty( ) )
				throw Exception( "LinkedList is empty" );
			else if( this->head == this->tail )
			{
				delete this->head;
				this->head = this->tail = NULL;
			}
			else
			{
				this->tail = this->tail->prev;
				delete this->tail->next;
				//
				this->tail->next = this->head;
				this->head->prev=this->tail;
			}
		}

		void removeFromPosition( int position )throw( Exception )
		{
			if( position <= 0 )
				throw Exception( "Invalid position" );
			else if( position == 1 )
				this->removeFirst();
			else if( position >= this->nodeCount( ) )
				this->removeLast();
			else
			{
				Node *trav = this->head;
				for( int count = 1; count < position - 1; ++ count )
					trav = trav->next;

				Node *ptrNode = trav->next;
				trav->next = ptrNode->next;
				ptrNode->next->prev = trav;
				delete ptrNode;
			}
		}
		void printList( void )throw( Exception )
		{
			if( this->empty( ) )
				throw Exception( "LinkedList is empty" );
			else
			{
				Node *trav = this->head;
				do
				{

					cout<<trav->data<<"	";
					trav = trav->next;
				}while( trav != head );
				cout<<endl;
			}
		}
		~LinkedList( void )
		{
			while( !this->empty( ) )
				this->removeFirst();
		}
	};
}
void accept_record( string message , int& data )
{
	cout<<message;
	cin>>data;
}
int menu_list( void )
{
	int choice;
	cout<<"0.Exit"<<endl;
	cout<<"1.Add First"<<endl;
	cout<<"2.Add Last"<<endl;
	cout<<"3.Add At Position"<<endl;
	cout<<"4.Remove First"<<endl;
	cout<<"5.Remove Last"<<endl;
	cout<<"6.Remove From Position"<<endl;
	cout<<"7.Print List"<<endl;
	cout<<"Enter choice	:	";
	cin>>choice;
	return choice;
}
int main( void )
{
	int choice, data, position ;
	using namespace collection;
	LinkedList list;
	while( ( choice = ::menu_list( ) ) != 0 )
	{
		try {
			switch( choice )
			{
			case 1:	//Add First
				accept_record("Enter data	:	", data);
				list.addFirst( data );
				break;
			case 2:	//Add Last
				accept_record("Enter data	:	", data);
				list.addLast( data );
				break;
			case 3:	//Add At Position
				accept_record("Enter position	:	", position);
				accept_record("Enter data	:	", data);
				list.addAtPosition( position, data );
				break;
			case 4:	//Remove First
				list.removeFirst( );
				break;
			case 5:	//Remove Last
				list.removeLast( );
				break;
			case 6:	//Remove From Position
				accept_record("Enter position	:	", position);
				list.removeFromPosition( position );
				break;
			case 7:
				list.printList( );
				break;
			case 8:
					cout<<	list.nodeCount()<<endl;
						break;
			}
		}
		catch (Exception &ex)
		{
			cout<<ex.getMessage()<<endl;
		}
		catch( bad_alloc &ex )
		{
			cout<<ex.what()<<endl;
		}

	}
	return 0;
}
