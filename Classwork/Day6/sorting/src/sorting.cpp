#include<iostream>
using namespace std;

#define SIZE	7

void print(int *arr)
{
	for (int i=0;i<SIZE;++i)
		cout<<arr[i]<<endl;
}
void bubble_sort (int *arr)
{
	bool flag=true;
	for (int i=0;i<SIZE-1 && flag==true;++i)
	{
		flag=false;
		for (int j=0;j<SIZE-1;++j)
			{
			flag=true;
			swap(arr[j],arr[j+1]);
			}
	}
}
void selection_sort (int *arr)
{

	for (int i=0;i<SIZE-1 ;++i)
	{
		for (int j=i+1;j<SIZE;++j)
			{
			if(arr[i]>arr[j])
			swap(arr[i],arr[j]);
			}
	}
}

void insertion_sort (int *arr)
{
	for( int i = 1; i < SIZE; ++ i )
		{
			int key = arr[ i ];
			int j  = i - 1;
			while( key < arr[ j ] && j >= 0 )
			{
				arr[ j + 1 ] = arr[ j ];
				-- j;
			}
			arr[ j + 1 ] = key;
		}
}

void merge( int *arr, int left, int mid, int right )
{
	int i = left, j = mid + 1, k = 0;
	int size = right - left + 1;
	int *temp = new int[ size ];
	while( i <= mid && j <= right )
	{
		if( arr[ i ] < arr[ j ] )
			temp[ k ++ ] = arr[ i ++ ];
		else
			temp[ k ++ ] = arr[ j ++ ];
	}
	while( i <= mid )
		temp[ k ++ ] = arr[ i ++ ];
	while( j <= right )
		temp[ k ++ ] = arr[ j ++ ];

	for( i = left, k = 0; i <= right; ++ i, ++ k  )
		arr[ i ] = temp[ k ];

	delete[] temp;
}
void merge_sort( int *arr, int left, int right )
{
	if( left < right )
	{
		int mid = ( left + right ) / 2;
		merge_sort(arr, left, mid);
		merge_sort(arr, mid + 1, right);
		merge( arr, left, mid, right );
	}
}
void quick_sort( int *arr, int left, int right )
{
	if( left > right )
		return;
	int pivot = arr[ left ];
	int i = left;
	int j = right;
	while( i <= j )
	{
		while( arr[ i ] <= pivot  )
			++i;
		while( arr[ j ] > pivot )
			-- j;
		if( i < j )
			swap( arr[ i ], arr[ j ] );
	}
	swap( arr[ left ], arr[ j ] );
	quick_sort(arr, left, j - 1 );
	quick_sort(arr, j + 1, right );
}

int main( void )
{
	int arr[ ] = { 103, 72, 151, 1, 2, 4, 234 };



	return 0;
}
