#include<iostream>
#include<string>

using namespace std;

class ArrayIndexOutOfBoundException
{
private:
	string message ;

public:
	ArrayIndexOutOfBoundException(string message):message(message)
{}
	string getMessage (void) const
	{
		return this->message;
	}


};
class Array
{
private:
	int size;
	int *arr;

public:
	Array():size(0),arr(NULL)
{}
	Array(int size)throw(bad_alloc)
		{
		this->size=size;
		this->arr=new int[this->size];
		}
	~Array()
	{
		if(this->arr!=NULL)
		{
			delete[] this->arr;
			this->arr=NULL;
		}
	}
//by binary search

	/*int find(int key)
		{
		int left=0,right=this->size-1;
		while(left <= right)
			{
			int mid=(left+right)/2;
				if(key==arr[mid])
					return mid;
				else if(key < arr[mid])
					right=mid-1;
				else
					left=mid+1;
			}
			return -1;
		}
*/

	//by linear search

	int find(int key)
	{
		for (int i=0;i<this->size;++i)
		{
			if(arr[i]==key)
				return i;
		}
		return -1;
	}


	friend istream& operator>> (istream &cin,Array &other)
	{
		for (int i=0;i<other.size;++i)
		{
			cout<<" arr [ "<<i<<"]";
			cin>>other.arr[i];
		}
		return cin;

	}

	friend ostream& operator<< (ostream &cout,Array &other)
		{
			for (int i=0;i<other.size;++i)

				cout<<" arr [ "<<i<<"] : "<<other.arr[i]<<endl;
				cout<<endl;

			return cout;

		}

};

int main( void )
{
  Array a1(5);
  cin>> a1;
  cout<<endl;
  cout<<a1;

  int key=5;
  int index=a1.find(key);
  if(index==-1)
	  cout<<"key not found";
  else
	  cout<<"key at "<<index;


  return 0;
}
