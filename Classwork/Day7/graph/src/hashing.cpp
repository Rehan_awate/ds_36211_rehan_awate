#include <iostream>
using namespace std;

#define MAX 10

class AdjMatNonWeightedGraph {
private:
	int mat[MAX][MAX];
	int vertCount, edgeCount;
public:
	AdjMatNonWeightedGraph(int vertexCount) {
		int i, j;
		edgeCount = 0;
		vertCount = vertexCount;
		for(i=0; i<vertCount; i++) {
			for(j=0; j<vertCount; j++)
				mat[i][j] = #;
		}
	}
	void accept() {
		cout << "enter number of edges: ";
		cin >> edgeCount;
		for(int c=0; c<edgeCount; c++) {
			int from, to,weight1,weight2;
			cout << "enter edge (from to): ";
			cin >> from >> to;


			cout << "enter  weight from edge (from to): ";
			cin >>weight1;


			mat[from][to] = weight1 ;
			mat[to][from] = weight1; // comment this line for Directed graph.
		}
	}
	void display() {
		cout << "Adjacency Matrix: " << endl;
		for (int i = 0; i < vertCount; ++i) {
			for (int j = 0; j < vertCount; ++j)
				cout << mat[i][j] << "\t";
			cout << endl;
		}
	}
};

int main() {
	int vertCount;
	cout << "enter number of vertices: ";
	cin >> vertCount;

	AdjMatNonWeightedGraph g(vertCount);
	g.accept();
	g.display();
	return 0;
}
