//============================================================================
// Name        : vector1.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <vector>
using namespace std;

int main() {
	vector<int> v;

	v.push_back(12);	v.push_back(22);	v.push_back(32);

	vector<int>::iterator itrstart =v.begin();
	vector<int>::iterator itrend =v.end();

	while(itrstart != itrend ){
		int element=*(itrstart);
		cout<<element;
		++ itrstart;

	}



	/*for (int n:v){
		cout<<n<<"\n";

	}*/


	return 0;
}
